/*
 *  Copyright (c) 2018, The OpenThread Authors.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the copyright holder nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef THIRD_PARTY_OPENTHREAD_INCLUDE_OPENTHREAD_CONFIG_FUCHSIA_H_
#define THIRD_PARTY_OPENTHREAD_INCLUDE_OPENTHREAD_CONFIG_FUCHSIA_H_

#ifdef OPENTHREAD_CONFIG_ANDROID_VERSION_HEADER_ENABLE
#include <openthread-config-android-version.h>
#endif

/* Define to 1 to no longer require the use of the va_list on otCliOutputCallback. */
#define OPENTHREAD_CONFIG_CLI_PREFORMAT_OUTPUT 1

/* Define to 1 to enable the DNS-SD Server API. */
#define OPENTHREAD_CONFIG_DNSSD_SERVER_ENABLE 1

/* Define to 1 to enable the SRP client API. */
#define OPENTHREAD_CONFIG_SRP_CLIENT_ENABLE 1

/* Define to 1 to enable the DNS client API. */
#define OPENTHREAD_CONFIG_DNS_CLIENT_ENABLE 1

/* Define to 1 to enable the DNS client service discovery API. */
#define OPENTHREAD_CONFIG_DNS_CLIENT_SERVICE_DISCOVERY_ENABLE 1

/* Define to 1 to enable the ping sender API. */
#define OPENTHREAD_CONFIG_PING_SENDER_ENABLE 1

/* Define to 0 to disable the CLI prompt. */
#define OPENTHREAD_CONFIG_CLI_PROMPT_ENABLE 0

/* Define to 1 to enable the border agent feature. */
#define OPENTHREAD_CONFIG_BORDER_AGENT_ENABLE 1

/* Define to 1 if you want to enable Border Router */
#define OPENTHREAD_CONFIG_BORDER_ROUTER_ENABLE 1

/* Define to 1 if you want to enable Border Routing */
#define OPENTHREAD_CONFIG_BORDER_ROUTING_ENABLE 1

/* Define to 1 if you want to enable platform UDP. */
#define OPENTHREAD_CONFIG_PLATFORM_UDP_ENABLE 1

/* Define to 1 if you want to enable TREL support */
#define OPENTHREAD_CONFIG_RADIO_LINK_TREL_ENABLE 1

/* Define to 1 if you want to enable SRP server support */
#define OPENTHREAD_CONFIG_SRP_SERVER_ENABLE 1

/* Define to 1 if you want to enable support for ECDSA */
#define OPENTHREAD_CONFIG_ECDSA_ENABLE 1

/* Define to 1 to enable OpenThread's TCP API */
#define OPENTHREAD_CONFIG_TCP_ENABLE 0

/* Define to 1 if you want to enable channel manager feature */
#define OPENTHREAD_CONFIG_CHANNEL_MANAGER_ENABLE 0

/* Define to 1 if you want to use channel monitor feature */
#define OPENTHREAD_CONFIG_CHANNEL_MONITOR_ENABLE 1

/* Define to 1 if you want to use child supervision feature */
#define OPENTHREAD_CONFIG_CHILD_SUPERVISION_ENABLE 1

/* Define to 1 to enable dtls support. */
#define OPENTHREAD_CONFIG_DTLS_ENABLE 1

/* Define to 1 if you want to use jam detection feature */
#define OPENTHREAD_CONFIG_JAM_DETECTION_ENABLE 1

/* Define to 1 to enable the joiner role. */
#define OPENTHREAD_CONFIG_JOINER_ENABLE 1

/* Define to 1 to enable being a commissioner. */
#define OPENTHREAD_CONFIG_COMMISSIONER_ENABLE 1

/* Define to 1 to enable the NCP HDLC interface. */
#define OPENTHREAD_CONFIG_NCP_HDLC_ENABLE 0

/* Define to 1 to enable posix platform. */
#define OPENTHREAD_PLATFORM_POSIX 0

/* Define to 1 if you want to enable Service */
#define OPENTHREAD_CONFIG_TMF_NETDATA_SERVICE_ENABLE 1

/* Define to 1 if you want to enable mac filter */
#define OPENTHREAD_CONFIG_MAC_FILTER_ENABLE 1

/* Define to 1 if you want to enable SLAAC address management */
#define OPENTHREAD_CONFIG_IP6_SLAAC_ENABLE 1

/* OpenThread examples */
#define OPENTHREAD_EXAMPLES none

/* OpenThread diagnostic feature */
#define OPENTHREAD_CONFIG_DIAG_ENABLE 1

/* OpenThread mac filter settings */
#define OPENTHREAD_CONFIG_MAC_FILTER_ENABLE 1

/* Allow OOB steering data to be set */
#define OPENTHREAD_CONFIG_MLE_STEERING_DATA_SET_OOB_ENABLE 1

/* The settings storage path on android. */
#define OPENTHREAD_CONFIG_POSIX_SETTINGS_PATH "/data/thread"

/* Enable radio coex capability */
#define OPENTHREAD_CONFIG_PLATFORM_RADIO_COEX_ENABLE 1

/* Allow the log level to be adjusted dynamicly */
#define OPENTHREAD_CONFIG_LOG_LEVEL_DYNAMIC_ENABLE 1

/* Set the default log level to be "note" */
#define OPENTHREAD_CONFIG_LOG_LEVEL OT_LOG_LEVEL_DEBG

/**
 * @def OPENTHREAD_CONFIG_IP6_MAX_EXT_MCAST_ADDRS
 *
 * The maximum number of supported IPv6 multicast addresses allows to be externally added.
 *
 */
#ifndef OPENTHREAD_CONFIG_IP6_MAX_EXT_MCAST_ADDRS
#define OPENTHREAD_CONFIG_IP6_MAX_EXT_MCAST_ADDRS 32
#endif

/**
 * @def OPENTHREAD_CONFIG_MLE_MAX_CHILDREN
 *
 * The maximum number of children.
 *
 */
#define OPENTHREAD_CONFIG_MLE_MAX_CHILDREN 128

/**
 * @def OPENTHREAD_CONFIG_PLATFORM_RADIO_SPINEL_RX_FRAME_BUFFER_SIZE
 *
 * Rx frame buffer in openthread
 *
 */
#define OPENTHREAD_CONFIG_PLATFORM_RADIO_SPINEL_RX_FRAME_BUFFER_SIZE 65535

// The number of outstanding message buffers allowed to be allocated.
#define OPENTHREAD_CONFIG_NUM_MESSAGE_BUFFERS 512

/**
 * @def OPENTHREAD_CONFIG_MLE_LINK_METRICS_SUBJECT_ENABLE
 *
 * Mandatory for 1.2+ FTD for 1.2 low power feature
 *
 */
#define OPENTHREAD_CONFIG_MLE_LINK_METRICS_SUBJECT_ENABLE 1

/**
 * @def OPENTHREAD_CONFIG_MLR_ENABLE
 *
 * Set to 1 to Enable Multicast Listener Registration
 *
 */
#define OPENTHREAD_CONFIG_MLR_ENABLE 1

/**
 * @def OPENTHREAD_CONFIG_MLR_ENABLE
 *
 * Set to 1 to enable Backbone Router features
 *
 */
#define OPENTHREAD_CONFIG_BACKBONE_ROUTER_ENABLE 1

/**
 * @def OPENTHREAD_CONFIG_MLR_ENABLE
 *
 * Set to 1 to Enable Multicast Routing on Backbone Link
 *
 */
#define OPENTHREAD_CONFIG_BACKBONE_ROUTER_MULTICAST_ROUTING_ENABLE 1

/**
 * @def OPENTHREAD_CONFIG_UPTIME_ENABLE
 *
 * Define to 1 to enable tracking the uptime of OpenThread instance.
 *
 */
#define OPENTHREAD_CONFIG_UPTIME_ENABLE 1

#endif // THIRD_PARTY_OPENTHREAD_INCLUDE_OPENTHREAD_CONFIG_FUCHSIA_H_
